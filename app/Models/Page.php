<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Page extends Model { //slmb -> snippet
   /**
     * The table associated with the model.
     * @var string
     */
    protected $table = 'pages';
}