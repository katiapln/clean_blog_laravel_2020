<?php
namespace App\Http\Controllers;
use App\Models\Page;
use Illuminate\Support\Facades\View;

class PagesController extends Controller {
  public function show(int $id = 1){
    $page = Page::find($id);
    return View::make('pages.show',compact('page'));
  }
}